import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART, INCREMENT, DECREMENT } from "../constants/shoeConstant";
const initialState = {
  shoes: data_shoes,
  gioHang: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      // console.log(payload);
      //do key-values trong đó value là object hoặc array thì clone
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => item.id === payload.id);
      if (index === -1) {
        cloneGioHang.push({ ...payload, soLuong: 1 });
      } else {
        cloneGioHang[index].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case INCREMENT: {
      // console.log(payload);
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => item.id === payload.id);
      cloneGioHang[index].soLuong++;
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case DECREMENT: {
      console.log(payload);
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => item.id === payload.id);
      if (cloneGioHang[index].soLuong === 1) {
        cloneGioHang.splice(index, 1);
      } else {
        cloneGioHang[index].soLuong--;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
