import { combineReducers } from "redux";
import { shoeReducer } from "./reducer/shoeReducer";

export const rootReducer_Ex_Shoes_redux = combineReducers({ shoeReducer });

//cách viết để đề xuất: viết trong dấu ngoặc tròn, sau đó thêm dấu ngoặc nhọn
// hoặc viết a: shoeReducer
