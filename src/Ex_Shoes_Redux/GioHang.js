import React, { Component } from "react";
import { connect } from "react-redux";
import { INCREMENT, DECREMENT } from "./redux/constants/shoeConstant";

class GioHang extends Component {
  renderTbody = () => {
    let { gioHang, increaseItem, decreaseItem } = this.props;
    return gioHang.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>${item.soLuong * item.price}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                increaseItem(item);
              }}
              className="btn btn-success"
            >
              +
            </button>
            <span className="px-5">{item.soLuong}</span>
            <button
              onClick={() => {
                decreaseItem(item);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.gioHang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    increaseItem: (item) => {
      dispatch({
        type: INCREMENT,
        payload: item,
      });
    },
    decreaseItem: (item) => {
      dispatch({
        type: DECREMENT,
        payload: item,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GioHang);

// java, .net

// nodejs  express

// js
