import React, { Component } from "react";
import { connect } from "react-redux";
import GioHang from "./GioHang";
import ItemShoe from "./ItemShoe";

class Ex_Shoes_Redux extends Component {
  renderContent = () => {
    let { shoe_list } = this.props;
    return shoe_list.map((item, index) => {
      return <ItemShoe key={index} data={item} />;
    });
  };
  render() {
    let { gioHang } = this.props;
    return (
      <div className="container py-5">
        {gioHang.length > 0 && <GioHang />}
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shoe_list: state.shoeReducer.shoes,
    gioHang: state.shoeReducer.gioHang,
  };
};

export default connect(mapStateToProps)(Ex_Shoes_Redux);
