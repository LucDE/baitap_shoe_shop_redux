import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import Ex_Layout_1 from "./Ex_Layout_1/Ex_Layout_1";
import Ex_Layout_2 from "./Ex_Layout_2/Ex_Layout_2";
import DataBinding from "./DataBinding/DataBinding";
import EventBinding from "./EventBinding/EventBinding";
import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import DemoState from "./DemoState/DemoState";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoProps from "./DemoProps/DemoProps";
import Ex_Shoes from "./Ex_Shoes/Ex_Shoes";
import Ex_Phone from "./Ex_Phone/Ex_Phone";
import Demo_Redux_Mini from "./Demo_Redux_Mini/Demo_Redux_Mini";
import Ex_Shoes_Redux from "./Ex_Shoes_Redux/Ex_Shoes_Redux";
import EX_Tai_Xiu from "./EX_Tai_Xiu/EX_Tai_Xiu";

function App() {
  return (
    <div className="App">
      {/* <DemoClass /> */}
      {/* <DemoClass /> */}
      {/* <DemoFunction /> */}
      {/* <Ex_Layout_1 /> */}
      {/* <Ex_Layout_2 /> */}
      {/* <DataBinding /> */}
      {/* <EventBinding /> */}
      {/* <ConditionalRendering /> */}
      {/* <DemoState /> */}
      {/* <div className="title">Helllo app</div> */}
      {/* <RenderWithMap /> */}
      {/* <DemoProps /> */}
      {/* <Ex_Shoes /> */}
      {/* <Ex_Phone /> */}
      {/* <Demo_Redux_Mini /> */}
      <Ex_Shoes_Redux />
      {/* <EX_Tai_Xiu /> */}
    </div>
  );
}

export default App;
